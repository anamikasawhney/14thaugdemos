import { Component } from '@angular/core';

@Component({
  selector: 'app-nopagefound',
  template: '<h1> There is no such page </h1>',
  styleUrls: ['./nopagefound.component.css']
})
export class NopagefoundComponent {

}
