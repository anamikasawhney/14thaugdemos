import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-departmentdetails',
  templateUrl: './departmentdetails.component.html',
  styleUrls: ['./departmentdetails.component.css']
})
export class DepartmentdetailsComponent implements OnInit {
constructor(private _activatedRoute : ActivatedRoute
  , private _router : Router){}
id : number = 0;
ngOnInit(): void {
  this.id = parseInt(this._activatedRoute.snapshot.params["id"]);
  console.log("ID rece in details "+ this.id);
}

goprevious()
{
  this.id--;
  this._router.navigate(['departmentdetails/'+ this.id]);
}

gonext()
{
  this.id++;
  this._router.navigate(['departmentdetails/'+ this.id]);
}

}
